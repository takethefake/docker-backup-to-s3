#!/bin/bash

set -e

echo "Job started: $(date)"

/usr/local/bin/s3cmd get $PARAMS "$S3_PATH"data.tgz data.tgz

tar xvf data.tgz 

echo "Job finished: $(date)"
