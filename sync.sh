#!/bin/bash

set -e

echo "Job started: $(date)"

tar czpvf data.tgz "$DATA_PATH"

/usr/local/bin/s3cmd put $PARAMS --server-side-encryption --region=$REGION data.tgz "$S3_PATH"

echo "Job finished: $(date)"
